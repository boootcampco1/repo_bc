using System;

namespace TwoLayerGameApi.Models
{
  public class Game
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Genre { get; set; }
    public string[] Platforms { get; set; }

    public override int GetHashCode()
    {
      return Id.GetHashCode();
    }
  }
}