using System;
using System.Collections.Generic;
using System.Linq;
using TwoLayerGameApi.Models;

namespace TwoLayerGameApi.Services
{
  class GamesService
  {
    private static List<Game> games = new List<Game> {
      new Game {Id = Guid.Parse("ab908249-7cf7-49cf-890e-d1c71cc08d59"), Name = "Space Invaders", Genre = "Arcade", Platforms = new string[] {"PSP", "ATARI", "NINTENDO"}},
      new Game {Id = Guid.Parse("f61e14e8-be77-4275-b764-57151f721381"), Name = "Mario Bros", Genre = "Platform", Platforms = new string[] {"NINTENDO"}},
      new Game {Id = Guid.Parse("88670aaf-bba4-4d87-8416-30456d5438c7"), Name = "Uncharted", Genre = "Adventure", Platforms = new string[] {"PS4"}},
    };

    public GamesService()
    {
    }
    public IEnumerable<Game> GetAllGames()
    {
      return games;
    }

    public Game GetGameById(Guid id)
    {
      return games.FirstOrDefault(game => game.Id == id);
    }

    public Game CreateGame(Game game)
    {
      game.Id = Guid.NewGuid();
      games.Add(game);
      return game;
    }

    public void UpdateGame(Guid id, Game game)
    {
      var gameToUpdate = games.FirstOrDefault(game => game.Id == id);
      if (gameToUpdate != null)
      {
        games.Remove(gameToUpdate);
        games.Add(game);
      }
    }

    public void DeleteGame(Guid id)
    {
      var gameToDelete = games.FirstOrDefault(game => game.Id == id);
      if (gameToDelete != null)
      {
        games.Remove(gameToDelete);
      }
    }
  }

}