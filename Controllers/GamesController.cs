﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TwoLayerGameApi.Models;
using TwoLayerGameApi.Services;

namespace TwoLayerGameApi.Controllers
{
  [ApiController]
  [Route("api/v1/games")]
  public class GamesController : ControllerBase
  {
    private readonly ILogger<GamesController> _logger;

    private readonly GamesService gamesService = new GamesService();

    public GamesController(ILogger<GamesController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IEnumerable<Game> GetAllGames()
    {
      return gamesService.GetAllGames();
    }

    [HttpGet("{id}")]
    public Game GetGameById(Guid id)
    {
      return gamesService.GetGameById(id);
    }

    [HttpPost]
    public Game CreateGame(Game game)
    {
      var newGame = gamesService.CreateGame(game);
      return newGame;
    }

    [HttpPut("{id}")]
    public void UpdateGame(Guid id, Game game)
    {
      gamesService.UpdateGame(id, game);
    }

    [HttpDelete("{id}")]
    public void DeleteGame(Guid id)
    {
      gamesService.DeleteGame(id);
    }

  }
}